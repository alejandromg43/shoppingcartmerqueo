package co.shoppingcartmerqueo.splash.view


import android.animation.Animator
import android.content.Intent
import android.os.Handler
import android.os.Looper
import co.shoppingcartmerqueo.R
import co.shoppingcartmerqueo.config.view.BaseActivity
import co.shoppingcartmerqueo.navigation.view.NavigationActivity
import kotlinx.android.synthetic.main.activity_splash.*


class SplashActivity : BaseActivity() {


    override fun findLayoutById() = R.layout.activity_splash
    override fun initUi() {
        splashscreenstart()
    }
    private fun splashscreenstart() {
        Handler(Looper.getMainLooper()).postDelayed({
            runOnUiThread {
                ivLogoSplash.animate()
                    .alpha(1.0f)
                    .setDuration(1000)
                    .setListener(object : Animator.AnimatorListener {
                        override fun onAnimationRepeat(animation: Animator?) {}
                        override fun onAnimationEnd(animation: Animator?) {
                            goToMain()
                        }

                        override fun onAnimationCancel(animation: Animator?) {}
                        override fun onAnimationStart(animation: Animator?) {}
                    }).start()
            }
        }, 200)
    }

    fun goToMain() {
        startActivity(Intent(this@SplashActivity, NavigationActivity::class.java))
        finish()
    }
}