package co.shoppingcartmerqueo.config.core.api.either

import co.shoppingcartmerqueo.config.core.api.exception.ApiException

open class ApiResult<out T: Any> {
    data class Success<out T : Any>(val data: T) : ApiResult<T>()
    data class Error(val exception: ApiException) : ApiResult<Nothing>()
}