package co.shoppingcartmerqueo.config.core.service

import co.shoppingcartmerqueo.home.api.ContentDataSource
import dagger.Module
import dagger.Provides
import dagger.Reusable
import retrofit2.Retrofit

@Module
class ShoppingCartProviderApiModule {
    @Provides @Reusable fun providerContentApi(retrofit: Retrofit): ContentDataSource =
            retrofit.create(ContentDataSource::class.java)

}

