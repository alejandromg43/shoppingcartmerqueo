package co.shoppingcartmerqueo.config.core.api.exception

import java.io.IOException

class NoConnectivityException(message: String): IOException(message)