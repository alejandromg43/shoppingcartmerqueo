package co.shoppingcartmerqueo.config.room


import android.content.Context
import androidx.room.Room
import co.shoppingcartmerqueo.home.room.DBShoppingCartRepository
import co.shoppingcartmerqueo.home.room.ShoppingCartDAO
import dagger.Module
import dagger.Provides
import dagger.Reusable

@Module
class RoomModule {

    private val DB = "shoppingcart.room.db"

    @Provides @Reusable fun providerDatabase(context: Context): ShoppingCartRoomDB =
        Room.databaseBuilder(context, ShoppingCartRoomDB::class.java, DB)
            .fallbackToDestructiveMigration()
            .build()

    @Provides @Reusable fun providerShoppingCartDAO(database: ShoppingCartRoomDB): ShoppingCartDAO =
        database.shoppingCartDAO()

    @Provides @Reusable fun repositoryShoppingCart(dao: ShoppingCartDAO): DBShoppingCartRepository =
        DBShoppingCartRepository(dao)
}