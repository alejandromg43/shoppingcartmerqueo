package co.shoppingcartmerqueo.config.room

import androidx.room.Database
import androidx.room.RoomDatabase
import co.shoppingcartmerqueo.home.model.Movies
import co.shoppingcartmerqueo.home.room.ShoppingCartDAO

@Database(entities = [
    Movies::class
], version = 1)
abstract class ShoppingCartRoomDB: RoomDatabase() {
    abstract fun shoppingCartDAO(): ShoppingCartDAO

}