package co.shoppingcartmerqueo.config.view

import androidx.lifecycle.Lifecycle
import androidx.lifecycle.ViewModelProvider
import dagger.android.support.DaggerAppCompatActivity
import javax.inject.Inject
import android.os.Bundle
import co.shoppingcartmerqueo.config.core.Injectable
import co.shoppingcartmerqueo.config.lifecycleowner.ShoppingCartLifecycleOwner


abstract class BaseActivity: DaggerAppCompatActivity(), Injectable {

    @Inject lateinit var shoppingCartViewModelFactory: ViewModelProvider.Factory
    protected val shoppingCartLifecycleOwner = ShoppingCartLifecycleOwner()

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(findLayoutById())
        shoppingCartLifecycleOwner.lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_CREATE)
        initUi()
    }

    override fun onResume() {
        super.onResume()
        shoppingCartLifecycleOwner.lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_RESUME)
    }

    override fun onDestroy() {
        super.onDestroy()
        shoppingCartLifecycleOwner.lifecycle.handleLifecycleEvent(Lifecycle.Event.ON_DESTROY)
    }


    protected abstract fun findLayoutById(): Int

    protected abstract fun initUi()

}