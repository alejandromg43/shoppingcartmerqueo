package co.shoppingcartmerqueo.config.lifecycleowner

import androidx.lifecycle.LifecycleOwner
import androidx.lifecycle.LifecycleRegistry

class ShoppingCartLifecycleOwner: LifecycleOwner {
    private val lifecycleRegistry = LifecycleRegistry(this)
    override fun getLifecycle() = lifecycleRegistry
}