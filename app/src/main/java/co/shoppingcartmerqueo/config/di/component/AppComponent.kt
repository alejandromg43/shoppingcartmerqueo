package co.shoppingcartmerqueo.config.di.component

import android.app.Application
import android.content.Context
import co.shoppingcartmerqueo.ShoppingCartApplication
import co.shoppingcartmerqueo.config.core.api.RetrofitInterceptor
import co.shoppingcartmerqueo.config.core.service.ShoppingCartProviderApiModule
import co.shoppingcartmerqueo.config.core.service.ShoppingCartProviderRepository
import co.shoppingcartmerqueo.config.di.binder.AppBinder
import co.shoppingcartmerqueo.config.di.retrofitmodule.RetrofitModule
import co.shoppingcartmerqueo.config.di.viewmodel.ViewModelModule
import co.shoppingcartmerqueo.config.room.RoomModule
import dagger.BindsInstance
import dagger.Component
import dagger.android.support.AndroidSupportInjectionModule
import javax.inject.Singleton

@Singleton
@Component(
        modules = [
            RetrofitModule::class,
            AndroidSupportInjectionModule::class,
            ViewModelModule::class,
            ShoppingCartProviderApiModule::class,
            ShoppingCartProviderRepository::class,
            RoomModule::class,
            AppBinder::class]
)
interface AppComponent {

    @Component.Builder
    interface Builder {
        @BindsInstance fun application(application: Application): Builder
        @BindsInstance fun context(context: Context): Builder
        fun build(): AppComponent
    }

    fun inject(shoppingCartApplication: ShoppingCartApplication)
    fun inject(retrofitInterceptor: RetrofitInterceptor)
}