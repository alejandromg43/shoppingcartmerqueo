package co.shoppingcartmerqueo.config.di.binder

import co.shoppingcartmerqueo.home.view.fragments.DetailMovieFragment
import co.shoppingcartmerqueo.home.view.fragments.HomeFragment
import co.shoppingcartmerqueo.home.view.fragments.ShoppingCartFragment
import co.shoppingcartmerqueo.navigation.view.NavigationActivity
import co.shoppingcartmerqueo.splash.view.SplashActivity

import dagger.Module
import dagger.android.ContributesAndroidInjector

@Suppress("unused")
@Module
abstract class AppBinder {
    @ContributesAndroidInjector
    abstract fun splashActivity(): SplashActivity

    @ContributesAndroidInjector
    internal abstract fun navigationActivity(): NavigationActivity

    @ContributesAndroidInjector
    internal abstract fun homeFragment(): HomeFragment

    @ContributesAndroidInjector
    internal abstract fun detailMovieFragment(): DetailMovieFragment

    @ContributesAndroidInjector
    internal abstract fun shoppingCartFragment(): ShoppingCartFragment

}