package co.shoppingcartmerqueo.config.di.viewmodel

import androidx.lifecycle.ViewModel
import androidx.lifecycle.ViewModelProvider
import co.shoppingcartmerqueo.config.core.viewmodel.ShoppingCartViewModelFactory
import co.shoppingcartmerqueo.config.core.viewmodel.ViewModelKey
import co.shoppingcartmerqueo.home.viewmodel.ContentViewModel
import dagger.Binds
import dagger.Module
import dagger.multibindings.IntoMap

@Suppress("unused")
@Module
abstract class ViewModelModule {
    @Binds
    abstract fun binViewModelFactory(factory: ShoppingCartViewModelFactory):
            ViewModelProvider.Factory

    @Binds
    @IntoMap
    @ViewModelKey(ContentViewModel::class)
    abstract fun bindViewModelContentViewModel(contentViewModel: ContentViewModel):
            ViewModel

}