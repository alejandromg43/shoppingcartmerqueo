package co.shoppingcartmerqueo.home.adapter

import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.shoppingcartmerqueo.R
import co.shoppingcartmerqueo.home.model.Movies
import co.shoppingcartmerqueo.utils.Constants.shoppingAction.Companion.ADDED_SHOPPING_CART
import co.shoppingcartmerqueo.utils.loadImage
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton

class ContentMoviesAdapter(
    var context: Context,
    var list: ArrayList<Movies>,
    var listener: ContentMoviesInterface
) : RecyclerView.Adapter<ContentMoviesAdapter.ViewHolder>() {


    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_content_list_movie, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contentItemData = list[position]
        holder.tvNameContent.text = contentItemData.title
        holder.itemView.setOnClickListener {
            listener.onItemSelect(
                contentItemData,
                holder.itemView
            )
        }
        holder.ivFlyer.loadImage(contentItemData.posterPath)
        holder.btAddMovie.setOnClickListener { listener.addItemCart(position) }
        holder.btRemoveMovie.setOnClickListener { listener.removeItemCart(position) }
        if (contentItemData.isMovieShoppingCart == ADDED_SHOPPING_CART) {
            holder.btRemoveMovie.show()
            holder.btAddMovie.hide()
        } else {
            holder.btRemoveMovie.hide()
            holder.btAddMovie.show()
        }

    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        val tvNameContent: TextView = itemview.findViewById(R.id.tvNameContent)
        val ivFlyer: ImageView = itemview.findViewById(R.id.ivFlyer)
        val btAddMovie: ExtendedFloatingActionButton = itemview.findViewById(R.id.btAddMovie)
        val btRemoveMovie: ExtendedFloatingActionButton = itemview.findViewById(R.id.btRemoveMovie)
    }


}
