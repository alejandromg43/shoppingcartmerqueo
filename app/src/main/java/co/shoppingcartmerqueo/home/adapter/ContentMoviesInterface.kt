package co.shoppingcartmerqueo.home.adapter

import android.view.View
import co.shoppingcartmerqueo.home.model.Movies

interface ContentMoviesInterface {
    fun onItemSelect(movies: Movies,view:View)
    fun addItemCart(position: Int)
    fun removeItemCart(position: Int)
}