package co.shoppingcartmerqueo.home.adapter


import android.content.Context
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import android.view.animation.Animation
import android.view.animation.ScaleAnimation
import android.widget.ImageView
import android.widget.TextView
import androidx.recyclerview.widget.RecyclerView
import co.shoppingcartmerqueo.R
import co.shoppingcartmerqueo.home.model.Movies
import co.shoppingcartmerqueo.utils.Constants.shoppingAction.Companion.ADDED_SHOPPING_CART
import co.shoppingcartmerqueo.utils.loadImage
import com.google.android.material.floatingactionbutton.ExtendedFloatingActionButton

class ShoppingCartAdapter(
    var context: Context,
    var list: ArrayList<Movies>,
    var listener: ContentMoviesInterface
) : RecyclerView.Adapter<ShoppingCartAdapter.ViewHolder>() {

    private val FADE_DURATION: Long = 1000

    override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ViewHolder {
        val v =
            LayoutInflater.from(parent.context)
                .inflate(R.layout.item_shoping_cart_list, parent, false)
        return ViewHolder(v)
    }

    override fun onBindViewHolder(holder: ViewHolder, position: Int) {
        val contentItemData = list[position]
        holder.tvNameContent.text = contentItemData.title
        holder.itemView.setOnClickListener {
            listener.onItemSelect(
                contentItemData,
                holder.itemView
            )
        }
        holder.ivFlyer.loadImage(contentItemData.backdropPath)
        holder.btAddMovie.setOnClickListener { listener.addItemCart(position) }
        holder.btRemoveMovie.setOnClickListener { listener.removeItemCart(position) }
        if (contentItemData.isMovieShoppingCart == ADDED_SHOPPING_CART) {
            holder.btRemoveMovie.show()
            holder.btAddMovie.hide()
        } else {
            holder.btRemoveMovie.hide()
            holder.btAddMovie.show()
        }
        setScaleAnimation(holder.itemView)
    }

    override fun getItemCount(): Int {
        return list.size
    }

    inner class ViewHolder(itemview: View) : RecyclerView.ViewHolder(itemview) {
        val tvNameContent: TextView = itemview.findViewById(R.id.tvNameContent)
        val ivFlyer: ImageView = itemview.findViewById(R.id.ivFlyer)
        val btAddMovie: ExtendedFloatingActionButton = itemview.findViewById(R.id.btAddMovie)
        val btRemoveMovie: ExtendedFloatingActionButton = itemview.findViewById(R.id.btRemoveMovie)

    }

    private fun setScaleAnimation(view: View) {
        val anim = ScaleAnimation(
            0.0f,
            1.0f,
            0.0f,
            1.0f,
            Animation.RELATIVE_TO_SELF,
            0.5f,
            Animation.RELATIVE_TO_SELF,
            0.5f
        )
        anim.duration = FADE_DURATION
        view.startAnimation(anim)
    }


}
