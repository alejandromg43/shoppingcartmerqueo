package co.shoppingcartmerqueo.home.repository

import co.shoppingcartmerqueo.config.core.api.either.ApiResult
import co.shoppingcartmerqueo.config.core.api.exception.ApiException
import co.shoppingcartmerqueo.config.core.api.response.ApiErrorResponse
import co.shoppingcartmerqueo.config.core.api.response.ApiErrorUnProcessableEntity
import co.shoppingcartmerqueo.config.core.api.response.ApiSuccessResponse
import co.shoppingcartmerqueo.home.api.ContentDataSource
import co.shoppingcartmerqueo.home.model.ContentMoviesResponse
import co.shoppingcartmerqueo.home.model.Movies
import kotlinx.coroutines.GlobalScope
import kotlinx.coroutines.async
import javax.inject.Inject

interface ContentRepository {

    suspend fun getMovies(page: String, category: String): ApiResult<ContentMoviesResponse>
    suspend fun getMoviesDetail(idMovie: String): ApiResult<Movies>

    class ContentNetwork @Inject constructor(private val contentDataSource: ContentDataSource) :
        ContentRepository {


        override suspend fun getMovies(
            page: String,
            category: String
        ): ApiResult<ContentMoviesResponse> = GlobalScope.async {
            val response =
                when (val apiResponse = contentDataSource.
                getMoviesAsync(category, page).await()) {
                    is ApiSuccessResponse ->
                        ApiResult.Success(apiResponse.data)

                    is ApiErrorResponse ->
                        ApiResult.Error(ApiException(apiResponse.errorMessage))

                    is ApiErrorUnProcessableEntity ->
                        ApiResult.Error(ApiException(apiResponse.errorMessage))

                    else ->
                        ApiResult.Error(ApiException("custom error response"))
                }

            response
        }.await()


        override suspend fun getMoviesDetail(idMovie: String): ApiResult<Movies> =
            GlobalScope.async {
                val response = when (val apiResponse =
                    contentDataSource.getMovieDetailAsync(idMovie).await()) {
                    is ApiSuccessResponse ->
                        ApiResult.Success(apiResponse.data)

                    is ApiErrorResponse ->
                        ApiResult.Error(ApiException(apiResponse.errorMessage))

                    is ApiErrorUnProcessableEntity ->
                        ApiResult.Error(ApiException(apiResponse.errorMessage))

                    else ->
                        ApiResult.Error(ApiException("custom error response"))
                }

                response
            }.await()

    }
}