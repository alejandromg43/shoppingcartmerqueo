package co.shoppingcartmerqueo.home.viewmodel

import androidx.lifecycle.LiveData
import androidx.lifecycle.MutableLiveData
import co.shoppingcartmerqueo.config.core.api.either.ApiResult
import co.shoppingcartmerqueo.config.core.viewmodel.CoroutinesViewModel
import co.shoppingcartmerqueo.home.model.ContentMoviesResponse
import co.shoppingcartmerqueo.home.model.Movies
import co.shoppingcartmerqueo.home.repository.ContentRepository
import co.shoppingcartmerqueo.home.room.DBShoppingCartRepository
import co.shoppingcartmerqueo.utils.Constants.categories.Companion.POPULAR
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.launch
import kotlinx.coroutines.withContext
import javax.inject.Inject


class ContentViewModel @Inject
constructor(
    private val contentRepository: ContentRepository,
    private val db: DBShoppingCartRepository
): CoroutinesViewModel() {

    private val _popularMovies = MutableLiveData<ContentMoviesResponse>()
    val listPopularMoviesObserver: LiveData<ContentMoviesResponse> get() = _popularMovies

    private val _movieDetail = MutableLiveData<Movies>()
    val movieDetailObserver: LiveData<Movies> get() = _movieDetail

    private val _moviesDao = MutableLiveData<List<Movies>>()
    val listMoviesDaoObserver: LiveData<List<Movies>> get() = _moviesDao

    fun getDetailMovie(idMovie: String) {
        uiScope.launch {
            when(val response = contentRepository.getMoviesDetail(idMovie)) {
                is ApiResult.Success<Movies> ->
                    _movieDetail.value = response.data
                is ApiResult.Error ->
                    _error.value = response.exception
            }
        }
    }

    fun getPopularMovies(page: String) {
        _loading.value = true
        uiScope.launch {
            when(val response = contentRepository.getMovies(page, POPULAR)) {
                is ApiResult.Success<ContentMoviesResponse> ->
                    _popularMovies.value = response.data
                is ApiResult.Error ->
                    _error.value = response.exception
            }
        }
        _loading.value = false
    }


    fun getMoviesLocal() {
        uiScope.launch {
            _moviesDao.value = db.dao.getAllMovies()
        }
    }

    fun getMovieDetailLocal( movieId:String) {
        uiScope.launch {
            _movieDetail.value = db.dao.getMovie(movieId)
        }
    }


     fun getShoppingCartList() {
        uiScope.launch(Dispatchers.IO) {
            _moviesDao.postValue(db.dao.getAllMoviesCart())
        }

    }

    fun cleanShoppingCart() {
        uiScope.launch {
            db.cleanShoppingCart()
        }
    }


    fun addAllMoviesLocal(listMovies:List<Movies>) {
        uiScope.launch {
            db.addAllMovies(listMovies)
        }
    }

    fun addMovieToCart(movie : Movies) {
        uiScope.launch {
            db.addMovieToCart(movie)
        }
    }

    fun removeMovieCart(movie : Movies) {
        uiScope.launch {
            db.removeMovieToCart(movie)
        }
    }
}