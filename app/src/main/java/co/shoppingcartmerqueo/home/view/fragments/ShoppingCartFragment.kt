package co.shoppingcartmerqueo.home.view.fragments

import android.os.Bundle
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.DividerItemDecoration
import androidx.recyclerview.widget.LinearLayoutManager
import co.shoppingcartmerqueo.R
import co.shoppingcartmerqueo.config.view.BaseFragment
import co.shoppingcartmerqueo.home.adapter.ContentMoviesInterface
import co.shoppingcartmerqueo.home.adapter.ShoppingCartAdapter
import co.shoppingcartmerqueo.home.model.Movies
import co.shoppingcartmerqueo.home.viewmodel.ContentViewModel
import co.shoppingcartmerqueo.utils.Constants
import co.shoppingcartmerqueo.utils.hide
import co.shoppingcartmerqueo.utils.setSafeOnClickListener
import co.shoppingcartmerqueo.utils.show
import kotlinx.android.synthetic.main.fragment_home.loaderMovies
import kotlinx.android.synthetic.main.fragment_home.panelEmpty
import kotlinx.android.synthetic.main.fragment_shopping_cart.*


class ShoppingCartFragment : BaseFragment(), ContentMoviesInterface {

    private lateinit var adapterListShoppingcart: ShoppingCartAdapter
    private var listShoppingCartMovies: ArrayList<Movies> = ArrayList()
    private lateinit var llManagerShoppingCart: LinearLayoutManager

    override fun findLayoutById() = R.layout.fragment_shopping_cart

    private val contentViewModel: ContentViewModel by viewModels {
        shoppingCartViewModelFactory
    }

    override fun initUi() {

        (activity as AppCompatActivity).supportActionBar?.hide()
        showLoader(true)
        initList()
        setObserver()
        getShoppingCart()
        uiListener()
    }

    fun uiListener() {
        btCleanCart?.setSafeOnClickListener { cleanShoppingCart() }
    }

    fun getShoppingCart() {
        contentViewModel.getShoppingCartList()
    }

    fun cleanShoppingCart() {
        contentViewModel.cleanShoppingCart()
        listShoppingCartMovies.clear()
        adapterListShoppingcart.notifyDataSetChanged()
    }

    fun initList() {
        llManagerShoppingCart = LinearLayoutManager(context, LinearLayoutManager.VERTICAL, false)
        adapterListShoppingcart =
            ShoppingCartAdapter(requireContext(), listShoppingCartMovies, this)
        rvShoppingCart.addItemDecoration(
            DividerItemDecoration(
                rvShoppingCart.context,
                DividerItemDecoration.VERTICAL
            )
        )
        rvShoppingCart.layoutManager = llManagerShoppingCart
        rvShoppingCart.adapter = adapterListShoppingcart
    }

    private fun setObserver() {
        contentViewModel.listMoviesDaoObserver.observe(shoppingCartLifecycleOwner, Observer {
            updateContetShoppingCartServed(it)
        })

        contentViewModel.errorResponse.observe(shoppingCartLifecycleOwner, Observer {
            if (it.message != null) showErrorDialog(it.message)
        })
    }


    fun showLoader(show: Boolean = false) {

        if (show)
            loaderMovies.show()
        else
            loaderMovies.hide()
    }

    private fun showEmptySplash(show: Boolean) {
        if (show)
            panelEmpty.show()
        else
            panelEmpty.hide()
    }

    private fun updateContetShoppingCartServed(list: List<Movies>) {
        listShoppingCartMovies.clear()
        listShoppingCartMovies.addAll(list)
        contentViewModel.addAllMoviesLocal(listShoppingCartMovies)
        showLoader(false)
        if (validateEmpty()) {
            showEmptySplash(true)
        } else {
            showEmptySplash(false)
        }
        adapterListShoppingcart.notifyDataSetChanged()
    }


    fun validateEmpty(): Boolean {
        return listShoppingCartMovies.isEmpty()
    }

    override fun onItemSelect(movie: Movies, view: View) {
        val data = Bundle()
        data.putString(Constants.keys.CONTENT_ID, movie.id)
        Navigation.findNavController(view)
            .navigate(R.id.action_shoppingCartFragment_to_detailMovieFragment, data)

    }

    override fun addItemCart(position: Int) {}

    override fun removeItemCart(position: Int) {
        contentViewModel.removeMovieCart(listShoppingCartMovies[position])
        updateMovieList(position)

    }

    fun updateMovieList(position: Int) {
        listShoppingCartMovies.removeAt(position)
        adapterListShoppingcart.notifyDataSetChanged()
    }
}
