package co.shoppingcartmerqueo.home.view.fragments

import android.graphics.Movie
import android.os.Bundle
import android.util.Log
import android.view.View
import androidx.appcompat.app.AppCompatActivity
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import androidx.navigation.Navigation
import androidx.recyclerview.widget.LinearLayoutManager
import co.shoppingcartmerqueo.R
import co.shoppingcartmerqueo.config.core.api.exception.ApiException
import co.shoppingcartmerqueo.config.view.BaseFragment
import co.shoppingcartmerqueo.home.adapter.ContentMoviesAdapter
import co.shoppingcartmerqueo.home.adapter.ContentMoviesInterface
import co.shoppingcartmerqueo.home.model.ContentMoviesResponse
import co.shoppingcartmerqueo.home.model.Movies
import co.shoppingcartmerqueo.home.viewmodel.ContentViewModel
import co.shoppingcartmerqueo.utils.Constants.keys.Companion.CONTENT_ID
import co.shoppingcartmerqueo.utils.Constants.shoppingAction.Companion.ADDED_SHOPPING_CART
import co.shoppingcartmerqueo.utils.Constants.shoppingAction.Companion.REMOVED_SHOPPING_CART
import co.shoppingcartmerqueo.utils.PaginationScrollListener
import co.shoppingcartmerqueo.utils.hide
import co.shoppingcartmerqueo.utils.setSafeOnClickListener
import co.shoppingcartmerqueo.utils.show
import kotlinx.android.synthetic.main.fragment_detail_movie.*
import kotlinx.android.synthetic.main.fragment_home.*


class HomeFragment : BaseFragment(), ContentMoviesInterface {

    private lateinit var adapterPopularMovies: ContentMoviesAdapter
    private var listPopularMovies: ArrayList<Movies> = ArrayList()
    private lateinit var llManagerPopular: LinearLayoutManager
    private var pagePopular: Int = 1
    private var TOTALPAGESPOPULAR: Int = 1
    private var isLoadingDataPopular: Boolean = false
    private var isLastPageDataPopular: Boolean = false

    override fun findLayoutById() = R.layout.fragment_home


    private val contentViewModel: ContentViewModel by viewModels {
        shoppingCartViewModelFactory
    }

    override fun initUi() {

        (activity as AppCompatActivity).supportActionBar?.hide()
        showLoader(true)
        uiListener()
        initList()
        setObserver()
        getPopularMovies()
    }

    fun uiListener() {
       btShoppingCart?.setSafeOnClickListener {v -> Navigation.findNavController(v).navigate(R.id.action_homeFragment_to_shoppingCartFragment) }
    }



    fun getPopularMovies() {
        contentViewModel.getPopularMovies(pagePopular.toString())
    }

    fun initList() {
        llManagerPopular = LinearLayoutManager(context, LinearLayoutManager.HORIZONTAL, false)
        adapterPopularMovies = ContentMoviesAdapter(requireContext(), listPopularMovies, this)
        rvMoviesPopular.layoutManager = llManagerPopular
        rvMoviesPopular.adapter = adapterPopularMovies
        addListenerPopularPaginator()
    }

    private fun setObserver() {
        contentViewModel.listPopularMoviesObserver.observe(shoppingCartLifecycleOwner, Observer {
            contentViewModel.addAllMoviesLocal(it.listContent as ArrayList<Movies>)
            getLocalMovies()
        })

        contentViewModel.listMoviesDaoObserver.observe(shoppingCartLifecycleOwner, Observer {
            Log.e("----",it.size.toString())
            updateContetPopularServed(it)
        })

        contentViewModel.errorResponse.observe(shoppingCartLifecycleOwner, Observer {
            captureError(it)
        })
    }


    fun captureError(error: ApiException){
        if (error.message != null) showErrorDialog(error.message)
        getLocalMovies()
    }

    fun getLocalMovies(){
        listPopularMovies.clear()
        contentViewModel.getMoviesLocal()
    }

    private fun addListenerPopularPaginator() {
        rvMoviesPopular.addOnScrollListener(object : PaginationScrollListener(llManagerPopular) {
            override fun loadMoreItems() {
                isLoadingDataPopular = true
                pagePopular += 1
                contentViewModel.getPopularMovies(pagePopular.toString())
            }
            override val totalPageCount: Int
                get() = TOTALPAGESPOPULAR

            override val isLastPage: Boolean
                get() = isLastPageDataPopular

            override val isLoading: Boolean
                get() = isLoadingDataPopular
        })
    }

    fun showLoader(show: Boolean = false) {

        if (show)
            loaderMovies.show()
        else
            loaderMovies.hide()
    }

    private fun showEmptySplash(show: Boolean) {

        if (show)
            panelEmpty.show()
        else
            panelEmpty.hide()
    }

    private fun updateContetPopularServed(list: List<Movies>) {
        listPopularMovies.clear()
        listPopularMovies.addAll(list)
        showLoader(false)
        if (validateEmpty()) {
            showEmptySplash(true)
        } else {
            showEmptySplash(false)
        }
        adapterPopularMovies.notifyDataSetChanged()
    }



    fun validateEmpty(): Boolean {
        return listPopularMovies.isEmpty() && pagePopular == 1
    }

    override fun onItemSelect(Movie: Movies, view: View) {
        val data = Bundle()
        data.putString(CONTENT_ID, Movie.id)
        Navigation.findNavController(view).navigate(R.id.action_homeFragment_to_detailMovieFragment,data)

        /* val fragment = DetailContentMovie()
         val data = Bundle()
         data.putString(CONTENT_ID, Movie.id)
         fragment.arguments = data
         goFragment(fragment, requireActivity(), R.id.fragment_container, MOVIES_DETAIL)

         */
    }

    override fun addItemCart(position: Int) {
        contentViewModel.addMovieToCart(listPopularMovies[position])
        updateMovieList(position,ADDED_SHOPPING_CART)

    }

    override fun removeItemCart(position: Int) {
        contentViewModel.removeMovieCart(listPopularMovies[position])
        updateMovieList(position,REMOVED_SHOPPING_CART)

    }

    fun updateMovieList(position : Int, isShoppingCart : Int){
        listPopularMovies[position].isMovieShoppingCart = isShoppingCart
        adapterPopularMovies.notifyDataSetChanged()
    }
}
