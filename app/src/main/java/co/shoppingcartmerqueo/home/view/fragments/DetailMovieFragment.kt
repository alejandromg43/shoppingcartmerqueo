package co.shoppingcartmerqueo.home.view.fragments

import android.annotation.SuppressLint
import android.content.Intent
import androidx.fragment.app.viewModels
import androidx.lifecycle.Observer
import co.shoppingcartmerqueo.R
import co.shoppingcartmerqueo.config.view.BaseFragment
import co.shoppingcartmerqueo.home.model.Movies
import co.shoppingcartmerqueo.home.viewmodel.ContentViewModel
import co.shoppingcartmerqueo.utils.Constants.deepLink.Companion.BASE_LINK
import co.shoppingcartmerqueo.utils.Constants.intentType.Companion.TEXT
import co.shoppingcartmerqueo.utils.Constants.keys.Companion.CONTENT_ID
import co.shoppingcartmerqueo.utils.hide
import co.shoppingcartmerqueo.utils.loadImage
import co.shoppingcartmerqueo.utils.setSafeOnClickListener
import co.shoppingcartmerqueo.utils.show
import kotlinx.android.synthetic.main.fragment_detail_movie.*

class DetailMovieFragment : BaseFragment() {


    var movieLoaded: Movies? = null
    lateinit var idMovie: String
    override fun findLayoutById() = R.layout.fragment_detail_movie
    private val contentViewModel: ContentViewModel by viewModels {
        shoppingCartViewModelFactory
    }

    override fun initUi() {
        setObserver()
        getArgumentsInit()
        uiListener()
    }

    private fun getArgumentsInit() {
        arguments?.getString(CONTENT_ID)?.let {
            getDataMovieDetail(it)
        }
    }

    private fun uiListener() {
        btShare.setSafeOnClickListener { shareCounter() }
    }

    private fun shareCounter() {
        val messageShare = getString(R.string.share_message) + " " + BASE_LINK + movieLoaded?.id
        val shareIntent = Intent()
        shareIntent.action = Intent.ACTION_SEND
        shareIntent.type = TEXT
        shareIntent.putExtra(Intent.EXTRA_TEXT, messageShare)
        startActivity(Intent.createChooser(shareIntent, getString(R.string.app_name)))
    }

    fun getDataMovieDetail(idMovieParameter: String) {
        showLoader(true)
        idMovie = idMovieParameter
        contentViewModel.getDetailMovie(idMovieParameter)
    }

    private fun setObserver() {
        contentViewModel.movieDetailObserver.observe(shoppingCartLifecycleOwner, Observer {
            setContent(it)
        })

        contentViewModel.errorResponse.observe(shoppingCartLifecycleOwner, Observer {
            contentViewModel.getMovieDetailLocal(idMovie)
        })
    }


    @SuppressLint("SetTextI18n")
    private fun setContent(it: Movies) {
        showLoader(false)
        movieLoaded = it
        tvTitleMovieDetail.text = it.title
        tvToolbarDetailContentMovies.text = it.title
        tvOverviewMovieDetail.text = it.overview
        tvCalificationDetailMovies.text = it.calification.toString()
        tvDuration.text = it.duration.toString()
        ivFlyerDetailMovies.loadImage(it.backdropPath)
    }

    private fun showLoader(show: Boolean = false) {
        if (show)
            loaderMovieDetail.show()
        else
            loaderMovieDetail.hide()
    }
}
