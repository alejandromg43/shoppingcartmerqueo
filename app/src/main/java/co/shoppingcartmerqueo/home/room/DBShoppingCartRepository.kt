package co.shoppingcartmerqueo.home.room

import co.shoppingcartmerqueo.home.model.Movies
import kotlinx.coroutines.CoroutineScope
import kotlinx.coroutines.Dispatchers
import kotlinx.coroutines.withContext
import kotlin.coroutines.CoroutineContext

class DBShoppingCartRepository(val dao: ShoppingCartDAO) : CoroutineScope {

    override val coroutineContext: CoroutineContext
        get() = Dispatchers.Main

    suspend fun addAllMovies(listMovies: List<Movies>) {
        withContext(Dispatchers.IO) {
            run {
                dao.addAllMovies(listMovies)
            }
        }
    }

    suspend fun addMovieToCart(movie: Movies) {
        withContext(Dispatchers.IO) {
            run {
                movie.isMovieShoppingCart = 1

                dao.updateMovie(movie)
            }
        }
    }

    suspend fun cleanShoppingCart() {
        withContext(Dispatchers.IO) {
            run {
                dao.cleanMovie()
            }
        }
    }

    suspend fun removeMovieToCart(movie: Movies) {
        withContext(Dispatchers.IO) {
            run {
                movie.isMovieShoppingCart = 0
                dao.updateMovie(movie)
            }
        }
    }
}