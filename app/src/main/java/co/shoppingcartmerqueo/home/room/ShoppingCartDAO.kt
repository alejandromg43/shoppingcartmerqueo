package co.shoppingcartmerqueo.home.room


import androidx.room.*
import co.shoppingcartmerqueo.home.model.Movies

@Dao
interface ShoppingCartDAO {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    fun addAllMovies(objects: List<Movies>)

    @Query("SELECT * FROM Movies WHERE isMovieShoppingCart = 1")
    fun getAllMoviesCart(): MutableList<Movies>

    @Query("SELECT * FROM Movies")
    suspend fun getAllMovies(): MutableList<Movies>

    @Update
    suspend fun updateMovie(movie: Movies)

    @Query("SELECT * FROM Movies WHERE id = :movieId")
    suspend fun getMovie(movieId: String): Movies?

    @Query("UPDATE Movies SET isMovieShoppingCart = 0")
    suspend fun cleanMovie()
}