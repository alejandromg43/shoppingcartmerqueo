package co.shoppingcartmerqueo.home.api

import co.shoppingcartmerqueo.config.core.api.response.ApiResponse
import co.shoppingcartmerqueo.home.model.ContentMoviesResponse
import co.shoppingcartmerqueo.home.model.Movies
import kotlinx.coroutines.Deferred
import retrofit2.http.GET
import retrofit2.http.Path
import retrofit2.http.Query

interface ContentDataSource {

    @GET("3/movie/{category}")
    fun getMoviesAsync(
        @Path("category") category: String,
        @Query("page") page: String
    ): Deferred<ApiResponse<ContentMoviesResponse>>


    @GET("3/movie/{id_movie}")
    fun getMovieDetailAsync(@Path("id_movie") idMovie: String): Deferred<ApiResponse<Movies>>


}