package co.shoppingcartmerqueo.home.model

import android.os.Parcelable
import androidx.room.Entity
import androidx.room.PrimaryKey
import com.google.gson.annotations.Expose
import com.google.gson.annotations.SerializedName
import kotlinx.android.parcel.Parcelize

@Parcelize
@Entity
open class Movies(
    @PrimaryKey @SerializedName("id") @Expose val id: String,
    @SerializedName("title") @Expose val title: String?,
    @SerializedName("poster_path") @Expose val posterPath: String,
    @SerializedName("backdrop_path") @Expose val backdropPath: String,
    @SerializedName("overview") @Expose val overview: String,
    @SerializedName("vote_average") @Expose val calification: Double,
    @SerializedName("runtime") @Expose val duration: Int,
    var isMovieShoppingCart: Int = 0
) : Parcelable