package co.shoppingcartmerqueo.utils

import android.annotation.SuppressLint
import android.view.View
import android.widget.ImageView
import androidx.fragment.app.Fragment
import androidx.fragment.app.FragmentActivity
import co.shoppingcartmerqueo.BuildConfig
import co.shoppingcartmerqueo.R
import co.shoppingcartmerqueo.config.view.BaseFragment
import com.squareup.picasso.Picasso
import java.text.SimpleDateFormat
import java.util.*

fun View.hide(){
    this.visibility = View.GONE
}

fun View.show(){
    this.visibility = View.VISIBLE
}

fun ImageView.loadImage(url: String) =
    Picasso.with(this.context)
        .load(BuildConfig.URL_IMAGES+url)
        .into(this)

fun View.setSafeOnClickListener(onSafeClick: (View) -> Unit) {
    val safeClickListener = SafeClickListener {
        onSafeClick(it)
    }
    setOnClickListener(safeClickListener)
}