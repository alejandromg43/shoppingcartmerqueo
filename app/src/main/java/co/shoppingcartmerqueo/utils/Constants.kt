package co.shoppingcartmerqueo.utils



class Constants {

    interface keys {
        companion object {
            const val CONTENT_ID = "CONTENT_ID"
        }
    }

    interface categories {
        companion object {
            const val POPULAR = "popular"
        }
    }

    interface shoppingAction {
        companion object {
            const val ADDED_SHOPPING_CART = 1
            const val REMOVED_SHOPPING_CART = 0

        }
    }

    interface intentType {
        companion object {
            const val TEXT = "text/plain"
        }
    }

    interface deepLink {
        companion object {
            const val BASE_LINK = "www.shoppingcart.com/"
        }
    }




}