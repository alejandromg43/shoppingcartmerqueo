package co.shoppingcartmerqueo.navigation.view

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import co.shoppingcartmerqueo.R

class NavigationActivity : AppCompatActivity() {
    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_navigation)
    }
}