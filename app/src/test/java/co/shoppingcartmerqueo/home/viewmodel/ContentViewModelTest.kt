package co.shoppingcartmerqueo.home.viewmodel

import androidx.arch.core.executor.testing.InstantTaskExecutorRule
import androidx.lifecycle.LiveData
import co.shoppingcartmerqueo.config.core.api.either.ApiResult
import co.shoppingcartmerqueo.config.core.api.exception.ApiException
import co.shoppingcartmerqueo.config.core.viewmodel.CoroutinesViewModel
import co.shoppingcartmerqueo.home.model.ContentMoviesResponse
import co.shoppingcartmerqueo.home.repository.ContentRepository
import co.shoppingcartmerqueo.home.room.DBShoppingCartRepository
import co.shoppingcartmerqueo.utils.Constants
import junit.framework.Assert.*
import kotlinx.coroutines.launch
import kotlinx.coroutines.runBlocking
import org.junit.Before
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.MockitoAnnotations
import org.mockito.Spy
import org.robolectric.RobolectricTestRunner
import org.robolectric.annotation.Config


@RunWith(RobolectricTestRunner::class)
@Config(manifest=Config.NONE)
class ContentViewModelTest : CoroutinesViewModel(){

    @get:Rule
    var instantExecutorRule = InstantTaskExecutorRule()

    private lateinit var viewModel: ContentViewModel

    private lateinit var isLoadingLiveData: LiveData<Boolean>

    private lateinit var isErrorLiveData: LiveData<ApiException>

    private  var pageService: String = "1"

    @Spy
    var counterListLiveData: ApiResult<ContentMoviesResponse>? = ApiResult()

    @Mock
    lateinit var repo: ContentRepository

    @Mock
    lateinit var db: DBShoppingCartRepository


    @Before
    fun setUp() {
        MockitoAnnotations.initMocks(this)
        uiScope.launch {
            `when`(repo.getMovies(pageService, Constants.categories.POPULAR)).thenReturn(counterListLiveData)

        }
        viewModel = ContentViewModel(repo,db)
        isLoadingLiveData = viewModel.loading
        isErrorLiveData = viewModel.errorResponse

    }

    @Test
    fun loadTeamsShouldShowAndHideLoading() = runBlocking {
        viewModel.getPopularMovies(pageService)
        var isLoading = isLoadingLiveData.value
        assertNotNull(isLoading)
        isLoading?.let { assertFalse(it) }
        return@runBlocking
    }


}