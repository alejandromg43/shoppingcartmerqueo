package co.shoppingcartmerqueo.home.view.fragments

import androidx.test.espresso.Espresso
import androidx.test.espresso.action.ViewActions.click
import androidx.test.espresso.assertion.ViewAssertions
import androidx.test.espresso.matcher.ViewMatchers
import androidx.test.espresso.matcher.ViewMatchers.withId
import androidx.test.ext.junit.rules.ActivityScenarioRule
import androidx.test.internal.runner.junit4.AndroidJUnit4ClassRunner
import co.shoppingcartmerqueo.R
import co.shoppingcartmerqueo.navigation.view.NavigationActivity
import org.junit.Rule
import org.junit.Test
import org.junit.runner.RunWith

@RunWith(AndroidJUnit4ClassRunner::class)
class HomeFragmentTest {

    @get:Rule
    val activityRule = ActivityScenarioRule(NavigationActivity::class.java)


    @Test
    fun test_isListFragmentVisible_onAppLaunch() {
        Espresso.onView(withId(R.id.rvMoviesPopular))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun test_shooping_cart_isFragmentVisible() {
        // Validamos que el boton del carrito en el home es visible
        Espresso.onView(withId(R.id.btShoppingCart))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun test_go_isShoppingCartFragmentVisible() {
        Espresso.onView(withId(R.id.btShoppingCart)).perform(click())

        // Validamos que el carrito de compras se muestra
        Espresso.onView(withId(R.id.btCleanCart))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

    @Test
    fun test_detail_isGoBack() {
        Espresso.onView(withId(R.id.btShoppingCart)).perform(click())

        Espresso.onView(withId(R.id.btCleanCart))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))

        Espresso.pressBack()
        // Validamos que navega bien atras desde carrito de compras
        Espresso.onView(withId(R.id.rvMoviesPopular))
            .check(ViewAssertions.matches(ViewMatchers.isDisplayed()))
    }

}